Mapbender bends a map along a path.

Example execution:

	./mapbender.py Weser-Radweg-Hauptroute.csv 0.286 6 20

The openstreetmap map of the Weser area is currently hardcoded. Upon execution
it will show the path given in Weser-Radweg-Hauptroute.csv on the map in a
matplotlib plot. It will show the approximated b-spline with the given
smoothing factor (6 above) and the map section of the given width (0.286 above)
around that curve. The area will be split into sections (20 in the example)
which will individually be transformed into rectangles which are also plotted.

On Debian systems you need the following packages:

	apt-get install python python-pil python-scipy python-tk python-matplotlib python-numpy


how to setup postgresql+postgis locally without root
----------------------------------------------------

/usr/lib/postgresql/14/bin/initdb -D /tmp/postgres
/usr/lib/postgresql/14/bin/postgres --port=5433 --unix_socket_directories=/tmp/postgres -D /tmp/postgres &
/usr/lib/postgresql/14/bin/createdb --port=5433 --host=/tmp/postgres gis
/usr/lib/postgresql/14/bin/psql --port=5433 --host=/tmp/postgres gis -c 'CREATE EXTENSION postgis;' -c 'CREATE EXTENSION hstore;'

osm2pgsql --port=5433 --host=/tmp/postgres -d gis --create --slim -G --hstore --tag-transform-script /tmp/openstreetmap-carto/openstreetmap-carto.lua -S /tmp/openstreetmap-carto/openstreetmap-carto.style ~/Downloads/map.xml
/usr/lib/postgresql/14/bin/psql --port=5433 --host=/tmp/postgres gis -f /tmp/openstreetmap-carto/indexes.sql

openstreetmap-carto:
    scripts/get-external-data.py --port=5433 --host=/tmp/postgres

diff --git a/project.mml b/project.mml
index 7fb3d47..d8014f8 100644
--- a/project.mml
+++ b/project.mml
@@ -27,6 +27,8 @@ _parts:
   osm2pgsql: &osm2pgsql
     type: "postgis"
     dbname: "gis"
+    port: "5433"
+    host: "/tmp/postgres"
     key_field: ""
     geometry_field: "way"
     extent: "-20037508,-20037508,20037508,20037508"


# the database connection settings are part of the style xml!
carto project.mml > mapnik.xml
nik4 --url https://www.openstreetmap.org/\#map\=12/49.7731/9.6726 /tmp/openstreetmap-carto/mapnik.xml screenshot.svg
